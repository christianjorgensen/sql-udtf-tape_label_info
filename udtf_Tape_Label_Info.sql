----------------------------------------------------------------------------------------------------
--                                                                                                --
--  Return Tape Label Details.                                                                    --
--                                                                                                --
--  Build instructions:                                                                           --
--                                                                                                --
-- *> RUNSQLSTM SRCSTMF('&FP') COMMIT(*NONE) DFTRDBCOL(&FCN2)                                  <* --
--                                                                                                --
----------------------------------------------------------------------------------------------------
--  2017-09-21 Christian Jorgensen                                                                --
--             Create user-defined table function.                                                --
--  2019-11-01 Christian Jorgensen                                                                --
--             Change Tape_Label_Info specific name to same as UDTF.                              --
----------------------------------------------------------------------------------------------------

create or replace function Tape_Label_Info ( Tape_Device varchar( 10 )
                                           , Volume_ID   varchar(  8 ) default '*MOUNTED'
                                           , File_label  varchar( 17 ) default '*ALL'
                                           , Starting_file_sequence_number varchar( 10 ) default '*FIRST'
                                           , Ending_file_sequence_number   varchar( 10 ) default '*LAST'
                                           , End_option  varchar(  7 ) default '*REWIND'
                                           )
  returns table(
      Device_name                                          varchar( 10 )
    , Cartridge_ID                                         varchar(  6 )
    , Label_identifier                                     varchar(  3 )
    , Label_number                                         varchar(  1 )
    , Volume_identifier                                    varchar(  6 )
    , Volume_access_security                               varchar(  3 )
    , Owner_identifier                                     varchar( 14 )
    , Encoding                                             varchar( 11 )
    , Standard_label                                       varchar(  3 )
    , Leading_tape_mark_tape                               varchar(  3 )
    , Density                                              varchar( 10 )
    , Additional_label_entries                             varchar(  3 )
    , Number_of_label_information_entries                  integer
    , Label_1_Label_identifier                             varchar(  3 )
    , Label_1_Label_number                                 varchar(  1 )
    , Data_set_identifier                                  varchar( 17 )
    , Aggregate_volume_identifier                          varchar(  6 )
    , Aggregate_volume_sequence_number                     integer
    , Data_set_sequence_number                             integer
    , Creation_date                                        date
    , Expiration_date                                      date
    , System_code                                          varchar( 13 )
    , Block_count                                          integer
    , Label_2_Label_identifier                             varchar(  3 )
    , Label_2_Label_number                                 varchar(  1 )
    , Record_format                                        varchar(  9 )
    , Block_length                                         integer
    , Record_length                                        integer
    , Tape_density_format                                  varchar(  8 )
    , Data_set_position                                    varchar( 19 )
    , Compressed                                           varchar(  3 )
    , Control_character                                    varchar(  7 )
    , Block_attribute                                      varchar( 19 )
    , Checkpoint_data_set_identifier                       varchar( 13 )
  )
specific Tape_Label_Info
language RPGLE
deterministic
modifies sql data
external name 'NMPAPI/TAPLABINF(Tape_Label_Info)'
parameter style SQL
not fenced
no final call
;
