**free
/if not defined( Copying_prototypes )
//-----------------------------------------------------------------------------------------------------------/
//                                                                                                           /
//  Brief description of collection of procedures.                                                           /
//                                                                                                           /
//  Procedures:                                                                                              /
//                                                                                                           /
//  Tape_Label_Info       - return info about tape labels.                                                   /
//                                                                                                           /
//  Compilation:                                                                                             /
//                                                                                                           /
//*>  CRTSQLRPGI OBJ(&FCN2/&FNR) SRCSTMF('&FP') OBJTYPE(*MODULE)                                           <*/
//*>  CRTSRVPGM  SRVPGM(&FCN2/&FNR) MODULE(&FCN2/&FNR) EXPORT(*ALL) TEXT('Tape_Label_Info')                <*/
//*>  DLTMOD     MODULE(&FCN2/&FNR)                                                                        <*/
//                                                                                                           /
//-----------------------------------------------------------------------------------------------------------/
//  2017-09-24 : Christian Jorgensen                                                                         /
//               Create module.                                                                              /
//-----------------------------------------------------------------------------------------------------------/

ctl-opt debug;
ctl-opt option( *Srcstmt : *NoDebugIO );
ctl-opt thread( *serialize );

ctl-opt nomain;

//----------------------------------------------------------------
// Exported procedures:

/endif

/if not defined( TAPLABINF_prototype_copied )

// Procedure:    Tape_Label_Info
// Description:  Return information about tape labels.

dcl-pr Tape_Label_Info extproc( *dclcase );
  // Incoming parameters
  p_Tape_Device                                          varchar( 10 ) const;
  p_Volume_ID                                            varchar(  8 ) const;
  p_File_label                                           varchar( 17 ) const;
  p_Starting_file_sequence_number                        varchar( 10 ) const;
  p_Ending_file_sequence_number                          varchar( 10 ) const;
  p_End_option                                           varchar(  7 ) const;
  // Returned parameters
  p_Device_name                                          varchar( 10 );
  p_Cartridge_ID                                         varchar(  6 );
  p_Label_identifier                                     varchar(  3 );
  p_Label_number                                         varchar(  1 );
  p_Volume_identifier                                    varchar(  6 );
  p_Volume_access_security                               varchar(  3 );
  p_Owner_identifier                                     varchar( 14 );
  p_Encoding                                             varchar( 11 );
  p_Standard_label                                       varchar(  3 );
  p_Leading_tape_mark_tape                               varchar(  3 );
  p_Density                                              varchar( 10 );
  p_Additional_label_entries                             varchar(  3 );
  p_Number_of_label_information_entries                  int( 10 );
  p_Label_1_Label_identifier                             varchar(  3 );
  p_Label_1_Label_number                                 varchar(  1 );
  p_Data_set_identifier                                  varchar( 17 );
  p_Aggregate_volume_identifier                          varchar(  6 );
  p_Aggregate_volume_sequence_number                     int( 10 );
  p_Data_set_sequence_number                             int( 10 );
  p_Creation_date                                        date;
  p_Expiration_date                                      date;
  p_System_code                                          varchar( 13 );
  p_Block_count                                          int( 10 );
  p_Label_2_Label_identifier                             varchar(  3 );
  p_Label_2_Label_number                                 varchar(  1 );
  p_Record_format                                        varchar(  9 );
  p_Block_length                                         int( 10 );
  p_Record_length                                        int( 10 );
  p_Tape_density_format                                  varchar(  8 );
  p_Data_set_position                                    varchar( 19 );
  p_Compressed                                           varchar(  3 );
  p_Control_character                                    varchar(  7 );
  p_Block_attribute                                      varchar( 19 );
  p_Checkpoint_data_set_identifier                       varchar( 13 );
  // Null indicators for incoming parameters
  n_Tape_Device                                          int( 5 ) const;
  n_Volume_ID                                            int( 5 ) const;
  n_File_label                                           int( 5 ) const;
  n_Starting_file_sequence_number                        int( 5 ) const;
  n_Ending_file_sequence_number                          int( 5 ) const;
  n_End_option                                           int( 5 ) const;
  // Null indicators for returned parameters
  n_Device_name                                          int( 5 );
  n_Cartridge_ID                                         int( 5 );
  n_Label_identifier                                     int( 5 );
  n_Label_number                                         int( 5 );
  n_Volume_identifier                                    int( 5 );
  n_Volume_access_security                               int( 5 );
  n_Owner_identifier                                     int( 5 );
  n_Encoding                                             int( 5 );
  n_Standard_label                                       int( 5 );
  n_Leading_tape_mark                                    int( 5 );
  n_Density                                              int( 5 );
  n_Additional_label_entries                             int( 5 );
  n_Number_of_label_information_entries                  int( 5 );
  n_Label_1_Label_identifier                             int( 5 );
  n_Label_1_Label_number                                 int( 5 );
  n_Data_set_identifier                                  int( 5 );
  n_Aggregate_volume_identifier                          int( 5 );
  n_Aggregate_volume_sequence_number                     int( 5 );
  n_Data_set_sequence_number                             int( 5 );
  n_Creation_date                                        int( 5 );
  n_Expiration_date                                      int( 5 );
  n_System_code                                          int( 5 );
  n_Block_count                                          int( 5 );
  n_Label_2_Label_identifier                             int( 5 );
  n_Label_2_Label_number                                 int( 5 );
  n_Record_format                                        int( 5 );
  n_Block_length                                         int( 5 );
  n_Record_length                                        int( 5 );
  n_Tape_density_format                                  int( 5 );
  n_Data_set_position                                    int( 5 );
  n_Compressed                                           int( 5 );
  n_Control_character                                    int( 5 );
  n_Block_attribute                                      int( 5 );
  n_Checkpoint_data_set_identifier                       int( 5 );

  // SQL parameters.
  Sql_State   char( 5 );
  Function    varchar(  517 ) const;
  Specific    varchar(  128 ) const;
  MsgText     varchar( 1000 );
  CallType    int( 5 )       const;
end-pr;

//----------------------------------------------------------------
// Exported data:

/define TAPLABINF_prototype_copied
/if defined( Copying_prototypes )
/eof

/endif
/endif

//----------------------------------------------------------------
// Constants:

dcl-c CALL_STARTUP    -2;
dcl-c CALL_OPEN       -1;
dcl-c CALL_FETCH       0;
dcl-c CALL_CLOSE       1;
dcl-c CALL_FINAL       2;

dcl-c PARM_NULL       -1;
dcl-c PARM_NOTNULL     0;

dcl-c DIGITS         '1234567890';

//----------------------------------------------------------------
// Data types:

dcl-ds Request_qualifiers_t template qualified;
  Requested_volume_identifier                      char(  6 );
  Requested_file_label                             char( 17 );
  Starting_file_sequence_number                    char( 10 );
  Ending_file_sequence_number                      char( 10 );
  End_option                                       char(  1 );
end-ds;

dcl-ds RLBL0100_t template qualified;
  Bytes_returned                                   int( 10 );
  Bytes_available                                  int( 10 );
  Device_name                                      char( 10 );
  Cartridge_ID                                     char( 6  );
  dcl-ds Volume_Label;
    Label_identifier                               char(  3 );
    Label_number                                   char(  1 );
    Volume_identifier                              char(  6 );
    Volume_access_security                         char(  1 );
    VTOC_pointer                                   char(  5 );       // Not used
    *n                                             char( 21 );
    Owner_identifier                               char( 14 );
    *n                                             char( 29 );
  end-ds;
  Code                                             char(  1 );
  Standard_label                                   char(  1 );
  Leading_tape_mark                                char(  1 );
  Density                                          char( 10 );
  *n                                               char(  2 );
  Additional_label_entries                         char(  1 );	
  Offset_to_label_information                      int( 10 );
  Number_of_label_information_entries              int( 10 );
  Length_of_label_information_entry                int( 10 );
end-ds;

dcl-ds Tape_Label_entry_t template qualified;
  dcl-ds Label_1;
    Label_identifier                               char(  3 );
    Label_number                                   char(  1 );
    Data_set_identifier                            char( 17 );
    Aggregate_volume_identifier                    char(  6 );
    Aggregate_volume_sequence_number               char(  4 );
    Data_set_sequence_number                       char(  4 );
    Generation_number                              char(  4 );       // Not used
    Generation_version_number                      char(  2 );       // Not used
    Creation_date                                  char(  6 );
    Expiration_date                                char(  6 );
    Data_set_security                              char(  1 );       // Not used
    Block_count_low_order                          char(  6 );       // trailer labels only
    System_code                                    char( 13 );       // trailer labels only
    Reserved                                       char(  3 );
    Block_count_high_order                         char(  4 );       // trailer labels only
  end-ds;
  dcl-ds Label_2;
    Label_identifier                               char(  3 );
    Label_number                                   char(  1 );
    Record_format                                  char(  1 );
    Block_length                                   char(  5 );
    Record_length                                  char(  5 );
    Tape_density_format                            char(  1 );
    Data_set_position                              char(  1 );
    Job_job_step_identification                    char( 17 );       // Not used
    Tape_recording_technique                       char(  2 );
    Control_character                              char(  1 );
    *n                                             char(  1 );
    Block_attribute                                char(  1 );
    *n                                             char(  3 );
    Device_serial_number                           char(  5 );       // Not used
    Checkpoint_data_set_identifier                 char(  1 );
    *n                                             char( 22 );
    Large_block_length                             char( 10 );
  end-ds;
  Logical_block_ID                                 char( 32 );
  Sequence_number                                  char( 10 );
  Multi_volume_sequence_number                     char( 10 );
  S36_file_type                                    char(  8 );
end-ds;

//----------------------------------------------------------------
// Global data:

// API error data structure:

dcl-ds ApiError;
  AeBytPrv  int( 10 )   inz( %size( ApiError ) );
  AeBytAvl  int( 10 );
  AeExcpId  char( 7 );
  *n        char( 1 );
  AeExcpDta char( 128 );
end-ds;

//----------------------------------------------------------------
// Prototypes:

/define Copying_prototypes
/undefine Copying_prototypes

//----------------------------------------------------------------
// Procedure:    Tape_Label_Info
// Description:  Return information about image catalog,

dcl-proc Tape_Label_Info export;

  dcl-pi *n;
    // Incoming parameters
    p_Tape_Device                                          varchar( 10 ) const;
    p_Volume_ID                                            varchar(  8 ) const;
    p_File_label                                           varchar( 17 ) const;
    p_Starting_file_sequence_number                        varchar( 10 ) const;
    p_Ending_file_sequence_number                          varchar( 10 ) const;
    p_End_option                                           varchar(  7 ) const;
    // Returned parameters
    p_Device_name                                          varchar( 10 );
    p_Cartridge_ID                                         varchar(  6 );
    p_Label_identifier                                     varchar(  3 );
    p_Label_number                                         varchar(  1 );
    p_Volume_identifier                                    varchar(  6 );
    p_Volume_access_security                               varchar(  3 );
    p_Owner_identifier                                     varchar( 14 );
    p_Encoding                                             varchar( 11 );
    p_Standard_label                                       varchar(  3 );
    p_Leading_tape_mark_tape                               varchar(  3 );
    p_Density                                              varchar( 10 );
    p_Additional_label_entries                             varchar(  3 );
    p_Number_of_label_information_entries                  int( 10 );
    p_Label_1_Label_identifier                             varchar(  3 );
    p_Label_1_Label_number                                 varchar(  1 );
    p_Data_set_identifier                                  varchar( 17 );
    p_Aggregate_volume_identifier                          varchar(  6 );
    p_Aggregate_volume_sequence_number                     int( 10 );
    p_Data_set_sequence_number                             int( 10 );
    p_Creation_date                                        date;
    p_Expiration_date                                      date;
    p_System_code                                          varchar( 13 );
    p_Block_count                                          int( 10 );
    p_Label_2_Label_identifier                             varchar(  3 );
    p_Label_2_Label_number                                 varchar(  1 );
    p_Record_format                                        varchar(  9 );
    p_Block_length                                         int( 10 );
    p_Record_length                                        int( 10 );
    p_Tape_density_format                                  varchar(  8 );
    p_Data_set_position                                    varchar( 19 );
    p_Compressed                                           varchar(  3 );
    p_Control_character                                    varchar(  7 );
    p_Block_attribute                                      varchar( 19 );
    p_Checkpoint_data_set_identifier                       varchar( 13 );
    // Null indicators for incoming parameters
    n_Tape_Device                                          int( 5 ) const;
    n_Volume_ID                                            int( 5 ) const;
    n_File_label                                           int( 5 ) const;
    n_Starting_file_sequence_number                        int( 5 ) const;
    n_Ending_file_sequence_number                          int( 5 ) const;
    n_End_option                                           int( 5 ) const;
    // Null indicators for returned parameters
    n_Device_name                                          int( 5 );
    n_Cartridge_ID                                         int( 5 );
    n_Label_identifier                                     int( 5 );
    n_Label_number                                         int( 5 );
    n_Volume_identifier                                    int( 5 );
    n_Volume_access_security                               int( 5 );
    n_Owner_identifier                                     int( 5 );
    n_Encoding                                             int( 5 );
    n_Standard_label                                       int( 5 );
    n_Leading_tape_mark                                    int( 5 );
    n_Density                                              int( 5 );
    n_Additional_label_entries                             int( 5 );
    n_Number_of_label_information_entries                  int( 5 );
    n_Label_1_Label_identifier                             int( 5 );
    n_Label_1_Label_number                                 int( 5 );
    n_Data_set_identifier                                  int( 5 );
    n_Aggregate_volume_identifier                          int( 5 );
    n_Aggregate_volume_sequence_number                     int( 5 );
    n_Data_set_sequence_number                             int( 5 );
    n_Creation_date                                        int( 5 );
    n_Expiration_date                                      int( 5 );
    n_System_code                                          int( 5 );
    n_Block_count                                          int( 5 );
    n_Label_2_Label_identifier                             int( 5 );
    n_Label_2_Label_number                                 int( 5 );
    n_Record_format                                        int( 5 );
    n_Block_length                                         int( 5 );
    n_Record_length                                        int( 5 );
    n_Tape_density_format                                  int( 5 );
    n_Data_set_position                                    int( 5 );
    n_Compressed                                           int( 5 );
    n_Control_character                                    int( 5 );
    n_Block_attribute                                      int( 5 );
    n_Checkpoint_data_set_identifier                       int( 5 );

    // SQL parameters.
    Sql_State   char( 5 );
    Function    varchar(  517 ) const;
    Specific    varchar(  128 ) const;
    MsgText     varchar( 1000 );
    CallType    int( 5 )       const;
  end-pi;

  dcl-pr QTARTLBL extpgm( 'QTARTLBL' );
    *n likeds( RLBL0100_t );                                         // Receiver variable
    *n int( 10 )                      const;                         // Length of receiver variable
    *n char( 8 )                      const;                         // Format name
    *n char( 10 )                     const;                         // Device description
    *n likeds( Request_qualifiers_t ) const options( *varsize );     // Request qualifiers
    *n int( 10 )                      const;                         // Length of request qualifiers
    *n char( 1024 )                   const options( *varsize );     // Error code
  end-pr;

  dcl-s  Tape_Device      like( p_Tape_Device );
  dcl-s  Volume_ID        like( p_Volume_ID );
  dcl-s  File_label       like( p_File_label );
  dcl-s  Starting_file_sequence_number  like( p_Starting_file_sequence_number );
  dcl-s  Ending_file_sequence_number    like( p_Ending_file_sequence_number );
  dcl-s  End_option       like( p_End_option );
  dcl-ds Save qualified static;                                      // Saved values from previous call.
    CallType                       like( CallType    );
    Tape_Device                    like( Tape_Device );
    Volume_ID                      like( Volume_ID   );
    File_label                     like( File_label  );
    Starting_file_sequence_number  like( p_Starting_file_sequence_number );
    Ending_file_sequence_number    like( p_Ending_file_sequence_number );
    End_option                     like( p_End_option );
  end-ds;

  dcl-ds TapLabHdr        likeds( RLBL0100_t ) based( ptrTapLabHdr );
  dcl-s  ptrTapLabHdr     pointer static;
  dcl-ds TapLabDtl        likeds( Tape_Label_entry_t ) based( ptrTapLabDtl );
  dcl-s  ptrTapLabDtl     pointer static;

  dcl-ds Request          likeds( Request_qualifiers_t );

  dcl-s  CurEntry         like( RLBL0100_t.Number_of_label_information_entries ) static;

  dcl-s  Buffer_size      int( 10 );

  dcl-ds ConvBin len( 4 ) qualified;
    uns4   uns( 10 ) pos( 1 ) inz;
    bin3   char( 3 ) pos( 2 );
  end-ds;

  // Start all header fields at not NULL.

  n_Device_name                                          = PARM_NOTNULL;
  n_Cartridge_ID                                         = PARM_NOTNULL;
  n_Label_identifier                                     = PARM_NOTNULL;
  n_Label_number                                         = PARM_NOTNULL;
  n_Volume_identifier                                    = PARM_NOTNULL;
  n_Volume_access_security                               = PARM_NOTNULL;
  n_Owner_identifier                                     = PARM_NOTNULL;
  n_Encoding                                             = PARM_NOTNULL;
  n_Standard_label                                       = PARM_NOTNULL;
  n_Leading_tape_mark                                    = PARM_NOTNULL;
  n_Density                                              = PARM_NOTNULL;
  n_Additional_label_entries                             = PARM_NOTNULL;
  n_Number_of_label_information_entries                  = PARM_NOTNULL;

  // Start all detail fields at NULL.

  n_Label_1_Label_identifier                             = PARM_NULL;
  n_Label_1_Label_number                                 = PARM_NULL;
  n_Data_set_identifier                                  = PARM_NULL;
  n_Aggregate_volume_identifier                          = PARM_NULL;
  n_Aggregate_volume_sequence_number                     = PARM_NULL;
  n_Data_set_sequence_number                             = PARM_NULL;
  n_Creation_date                                        = PARM_NULL;
  n_Expiration_date                                      = PARM_NULL;
  n_System_code                                          = PARM_NULL;
  n_Block_count                                          = PARM_NULL;
  n_Label_2_Label_identifier                             = PARM_NULL;
  n_Label_2_Label_number                                 = PARM_NULL;
  n_Record_format                                        = PARM_NULL;
  n_Block_length                                         = PARM_NULL;
  n_Record_length                                        = PARM_NULL;
  n_Tape_density_format                                  = PARM_NULL;
  n_Data_set_position                                    = PARM_NULL;
  n_Compressed                                           = PARM_NULL;
  n_Control_character                                    = PARM_NULL;
  n_Block_attribute                                      = PARM_NULL;
  n_Checkpoint_data_set_identifier                       = PARM_NULL;

  //  Open, fetch & close...

  select;
    when  CallType = CALL_OPEN;

      // Verify that tape device was specified.

      if ( n_Tape_Device = PARM_NULL ) or
         ( %trim( p_Tape_Device ) = '' );
          SQL_State = '38999';
          MsgText = 'Tape device must be specified';
          return;
      else;
        Tape_Device = p_Tape_Device;
      endif;

      // Verify that valid volume ID was specified.

      exec SQL values upper( :p_Volume_ID ) into :Volume_ID;

      if ( %trim( Volume_ID ) = '' ) or ( Volume_ID = '*MOUNTED' );
        Request.Requested_volume_identifier = '';
      elseif ( %len( Volume_ID ) <= %size( Request.Requested_volume_identifier ) );
        Request.Requested_volume_identifier = Volume_ID;
      else;
        SQL_State = '38999';
        MsgText = 'Volume ID can not be longer than 6 characters.';
        return;
      endif;

      // Verify that file label was specified.

      exec SQL values upper( :p_File_label ) into :File_label;

      if ( %trim( File_label ) = '' ) or ( File_label = '*ALL' );
        Request.Requested_file_label = '*ALL';
      else;
        Request.Requested_file_label = p_File_label;
      endif;

      // Verify that starting sequence number is valid.

      exec SQL values upper( :p_Starting_file_sequence_number ) into :Starting_file_sequence_number;

      if ( %trim( Starting_file_sequence_number ) = '' ) or ( Starting_file_sequence_number = '*FIRST' );
        Request.Starting_file_sequence_number = '*FIRST';
      elseif ( %check( DIGITS : %trimr( Starting_file_sequence_number ) ) = 0 );
        Request.Starting_file_sequence_number = Starting_file_sequence_number;
      else;
        SQL_State = '38999';
        MsgText = 'Starting sequence number must be *FIRST or contain only digits.';
        return;
      endif;

      // Verify that ending sequence number is valid.

      exec SQL values upper( :p_Ending_file_sequence_number ) into :Ending_file_sequence_number;

      if ( %trim( Ending_file_sequence_number ) = '' ) or ( Ending_file_sequence_number = '*ONLY' );
        Request.Ending_file_sequence_number = '*ONLY';
      elseif ( Ending_file_sequence_number = '*LAST' );
        Request.Ending_file_sequence_number = Ending_file_sequence_number;
      elseif ( %check( DIGITS : %trimr( Ending_file_sequence_number ) ) = 0 );
        Request.Ending_file_sequence_number = Ending_file_sequence_number;
      else;
        SQL_State = '38999';
        MsgText = 'Ending sequence number must be *ONLY, *LAST or contain only digits.';
        return;
      endif;

      // Verify that end option is valid.

      exec SQL values upper( :p_End_option ) into :End_option;

      if ( %trim( End_option ) = '' ) or ( End_option = '*REWIND' );
        Request.End_option = '0';
      elseif ( End_option = '*UNLOAD' );
        Request.End_option = '1';
      elseif ( End_option = '*LEAVE' );
        Request.End_option = '2';
      else;
        SQL_State = '38999';
        MsgText = 'End option must be one of special values *REWIND, *UNLOAD or *LEAVE.';
        return;
      endif;

      // Get tape label basic info.

      ptrTapLabHdr = %alloc( %size( TapLabHdr ) );
      ptrTapLabDtl = *null;

      monitor;
        QTARTLBL( TapLabHdr
                : %size( TapLabHdr )
                : 'RLBL0100'
                : Tape_Device
                : Request
                : %size( Request )
                : x'00000000'               // ApiError
                );

      on-error *all;
        RcvMsg( AeExcpID : MsgText );
        if ( AeExcpID = 'CPF67D2' ); // CPF67D2 returns EOF...
          SQL_State = '02000';
          dealloc ptrTapLabHdr;
          return;
        else;
          SQL_State = '38999';
          MsgText = 'Error ' + AeExcpID + ': ' + MsgText + '. Please check joblog.';
          return;
        endif;
      endmon;

      // Expand buffer and get tape label entries.

      Buffer_size = TapLabHdr.Bytes_Available;
      ptrTapLabHdr = %realloc( ptrTapLabHdr : Buffer_size );

      monitor;
        QTARTLBL( TapLabHdr
                : Buffer_size
                : 'RLBL0100'
                : Tape_Device
                : Request
                : %len( Request )
                : x'00000000'               // ApiError
                );

      on-error *all;
        RcvMsg( AeExcpID : MsgText );
        SQL_State = '38999';
        MsgText = 'Error ' + AeExcpID + ': ' + MsgText + '. Please check joblog.';
        return;
      endmon;

      ptrTapLabDtl = ptrTapLabHdr + TapLabHdr.Offset_to_label_information;

      // Reset current entry before fetching.

      CurEntry = 0;

    when  CallType = CALL_FETCH;

      // If previous call was for fetch and no more entries, return EOF.

      if ( CallType                        = Save.CallType                      ) and
         ( p_Tape_Device                   = Save.Tape_Device                   ) and
         ( p_Volume_ID                     = Save.Volume_ID                     ) and
         ( p_File_label                    = Save.File_label                    ) and
         ( p_Starting_file_sequence_number = Save.Starting_file_sequence_number ) and
         ( p_Ending_file_sequence_number   = Save.Ending_file_sequence_number   ) and
         ( p_End_option                    = Save.End_option                    ) and
         ( CurEntry                        = TapLabHdr.Number_of_label_information_entries );
        SQL_State = '02000';
        return;
      endif;

      // Copy tape label header data to parameters.

      p_Device_name                                 = %trimr( TapLabHdr.Device_name                              );
      p_Cartridge_ID                                = %trimr( TapLabHdr.Cartridge_ID                             );
      p_Label_identifier                            = %trimr( TapLabHdr.Volume_Label.Label_identifier            );
      p_Label_number                                = %trimr( TapLabHdr.Volume_Label.Label_number                );
      p_Volume_identifier                           = %trimr( TapLabHdr.Volume_Label.Volume_identifier           );

      if ( TapLabHdr.Volume_Label.Volume_access_security = ' ' ) or
         ( TapLabHdr.Volume_Label.Volume_access_security = '0' ) or
         ( TapLabHdr.Volume_Label.Volume_access_security = x'00' );
        p_Volume_access_security = 'YES';
      else;
        p_Volume_access_security = 'NO';
      endif;

      p_Owner_identifier                            = %trimr( TapLabHdr.Volume_Label.Owner_identifier            );

      select;
        when ( TapLabHdr.Code = '0' );
          p_Encoding = 'ASCII';
        when ( TapLabHdr.Code = '1' );
          p_Encoding = 'EBCDIC';
        when ( TapLabHdr.Code = ' ' );
          p_Encoding = 'NON-LABELED';
      endsl;

      select;
        when ( TapLabHdr.Standard_label = '0' );
          p_Standard_label = 'NO';
        when ( TapLabHdr.Standard_label = '1' );
          p_Standard_label = 'YES';
      endsl;

      select;
        when ( TapLabHdr.Leading_tape_mark = '0' );
          p_Leading_tape_mark_tape = 'NO';
        when ( TapLabHdr.Leading_tape_mark = '1' );
          p_Leading_tape_mark_tape = 'YES';
      endsl;

      p_Density                                     = %trimr( TapLabHdr.Density                                  );

      select;
        when ( TapLabHdr.Additional_label_entries = '0' );
          p_Additional_label_entries = 'NO';
        when ( TapLabHdr.Additional_label_entries = '1' );
          p_Additional_label_entries = 'YES';
      endsl;

      p_Number_of_label_information_entries         = TapLabHdr.Number_of_label_information_entries               ;

      // If there are tape label entries, get next entry.

      if ( TapLabHdr.Number_of_label_information_entries > 0 );
        n_Label_1_Label_identifier                             = PARM_NOTNULL;
        n_Label_1_Label_number                                 = PARM_NOTNULL;
        n_Data_set_identifier                                  = PARM_NOTNULL;
        n_Aggregate_volume_identifier                          = PARM_NOTNULL;
        n_Aggregate_volume_sequence_number                     = PARM_NOTNULL;
        n_Data_set_sequence_number                             = PARM_NOTNULL;
        n_Creation_date                                        = PARM_NOTNULL;
        n_Expiration_date                                      = PARM_NOTNULL;
        n_System_code                                          = PARM_NOTNULL;
        n_Block_count                                          = PARM_NOTNULL;
        n_Label_2_Label_identifier                             = PARM_NOTNULL;
        n_Label_2_Label_number                                 = PARM_NOTNULL;
        n_Record_format                                        = PARM_NOTNULL;
        n_Block_length                                         = PARM_NOTNULL;
        n_Record_length                                        = PARM_NOTNULL;
        n_Tape_density_format                                  = PARM_NOTNULL;
        n_Data_set_position                                    = PARM_NOTNULL;
        n_Compressed                                           = PARM_NOTNULL;
        n_Control_character                                    = PARM_NOTNULL;
        n_Block_attribute                                      = PARM_NOTNULL;
        n_Checkpoint_data_set_identifier                       = PARM_NOTNULL;

        CurEntry += 1;
        ptrTapLabDtl = ptrTapLabHdr + TapLabHdr.Offset_to_label_information
                                    + ( TapLabHdr.Length_of_label_information_entry * ( CurEntry - 1 ) );


        // Copy tape label entry data to parameters.

        // ...label 1

        p_Label_1_Label_identifier                    = %trimr( TapLabDtl.Label_1.Label_identifier                 );
        p_Label_1_Label_number                        = %trimr( TapLabDtl.Label_1.Label_number                     );
        p_Data_set_identifier                         = %trimr( TapLabDtl.Label_1.Data_set_identifier              );
        p_Aggregate_volume_identifier                 = %trimr( TapLabDtl.Label_1.Aggregate_volume_identifier      );
        p_Aggregate_volume_sequence_number            = %int( TapLabDtl.Label_1.Aggregate_volume_sequence_number   );

        if ( %subst( TapLabDtl.Label_1.Data_set_sequence_number : 1 : 1 ) = x'6F' );
          ConvBin.bin3 = %subst( TapLabDtl.Label_1.Data_set_sequence_number : 2 : 3 );
          p_Data_set_sequence_number = ConvBin.uns4;
        else;
          p_Data_set_sequence_number = %int( TapLabDtl.Label_1.Data_set_sequence_number );
        endif;

        monitor;
          if ( %subst( TapLabDtl.Label_1.Creation_date : 1 : 1 ) = ' ' );
            p_Creation_date = %date( %char( 1900000 + %int( TapLabDtl.Label_1.Creation_date ) ) : *LONGJUL0 );
          else;
            p_Creation_date = %date( %char( 2000000 + %int( TapLabDtl.Label_1.Creation_date ) ) : *LONGJUL0 );
          endif;
        on-error *all;
          n_Creation_date = PARM_NULL;
        endmon;

        monitor;
          if ( TapLabDtl.Label_1.Expiration_date = '999999' );
            p_Expiration_date = *hival;
          else;
            if ( %subst( TapLabDtl.Label_1.Expiration_date : 1 : 1 ) = ' ' );
              p_Expiration_date = %date( %char( 1900000 + %int( TapLabDtl.Label_1.Expiration_date ) ) : *LONGJUL0 );
            else;
              p_Expiration_date = %date( %char( 2000000 + %int( TapLabDtl.Label_1.Expiration_date ) ) : *LONGJUL0 );
            endif;
          endif;
        on-error *all;
          n_Expiration_date = PARM_NULL;
        endmon;

        p_System_code                                 = %trimr( TapLabDtl.Label_1.System_code                      );

        if ( p_Label_1_Label_identifier = 'EOF') or ( p_Label_1_Label_identifier = 'EOV' );
          p_Block_count = %int( TapLabDtl.Label_1.Block_count_high_order + TapLabDtl.Label_1.Block_count_low_order );
        else;
          p_Block_count = 0;
        endif;

        // ...label 2

        p_Label_2_Label_identifier                    = %trimr( TapLabDtl.Label_2.Label_identifier                 );
        p_Label_2_Label_number                        = %trimr( TapLabDtl.Label_2.Label_number                     );

        select;
          when ( TapLabDtl.Label_2.Record_format = 'F' );
            p_Record_format = 'FIXED';
          when ( TapLabDtl.Label_2.Record_format = 'V' );
            p_Record_format = 'VARIABLE';
          when ( TapLabDtl.Label_2.Record_format = 'U' );
            p_Record_format = 'UNDEFINED';
        endsl;

        p_Block_length                                = %int( TapLabDtl.Label_2.Large_Block_length                 );
        p_Record_length                               = %int( TapLabDtl.Label_2.Record_length                      );

        select;
          when ( TapLabDtl.Label_2.Tape_density_format = '3' );
            p_Tape_density_format = '1600 bpi';
          when ( TapLabDtl.Label_2.Tape_density_format = '4' );
            p_Tape_density_format = '6250 bpi';
          when ( TapLabDtl.Label_2.Tape_density_format = '5' );
            p_Tape_density_format = '3200 bpi';
          other;
            p_Tape_density_format = 'OTHER';
        endsl;

        select;
          when ( TapLabDtl.Label_2.Data_set_position = '0' );
            p_Data_set_position = 'VOLUME_NOT_SWITCHED';
          when ( TapLabDtl.Label_2.Data_set_position = '1' );
            p_Data_set_position = 'VOLUME_SWITCHED';
        endsl;

        select;
          when ( TapLabDtl.Label_2.Tape_recording_technique = ' ' );
            p_Compressed = 'NO';
          when ( TapLabDtl.Label_2.Tape_recording_technique = 'P' );
            p_Compressed = 'YES';
        endsl;

        select;
          when ( TapLabDtl.Label_2.Control_character = 'A' );
            p_Control_character = 'ANSI';
          when ( TapLabDtl.Label_2.Control_character = 'M' );
            p_Control_character = 'MACHINE';
          when ( TapLabDtl.Label_2.Control_character = ' ' );
            p_Control_character = 'NONE';
        endsl;

        select;
          when ( TapLabDtl.Label_2.Block_attribute = 'B' );
            p_Block_attribute = 'BLOCKED';
          when ( TapLabDtl.Label_2.Block_attribute = 'S' );
            p_Block_attribute = 'SPANNED';
          when ( TapLabDtl.Label_2.Block_attribute = 'R' );
            p_Block_attribute = 'BLOCKED_AND_SPANNED';
          when ( TapLabDtl.Label_2.Block_attribute = ' ' );
            p_Block_attribute = 'NONE';
        endsl;

        select;
          when ( TapLabDtl.Label_2.Checkpoint_data_set_identifier = 'C' );
            p_Checkpoint_data_set_identifier = 'CHECKPOINT';
          when ( TapLabDtl.Label_2.Checkpoint_data_set_identifier = '' );
            p_Checkpoint_data_set_identifier = 'NO_CHECKPOINT';
        endsl;
      endif;

    when  CallType = CALL_CLOSE;
      if ( ptrTapLabHdr <> *null );
        dealloc ptrTapLabHdr;
      endif;

  endsl;

  Save.CallType                      = CallType                       ;
  Save.Tape_Device                   = p_Tape_Device                  ;
  Save.Volume_ID                     = p_Volume_ID                    ;
  Save.File_label                    = p_File_label                   ;
  Save.Starting_file_sequence_number = p_Starting_file_sequence_number;
  Save.Ending_file_sequence_number   = p_Ending_file_sequence_number  ;
  Save.End_option                    = p_End_option                   ;

  return;

end-proc;

//----------------------------------------------------------------
// Procedure:    RcvMsg
// Description:  Receive message.

dcl-proc RcvMsg;
  dcl-pi *n ind;
    MsgID     char( 7 );
    MsgText   varchar( 1000 );
  end-pi;

  dcl-pr QMHRCVPM extpgm( 'QMHRCVPM' );
    *n likeds( RCVM0200 )             options( *varsize );           // Receiver variable
    *n int( 10 )                      const;                         // Length of receiver variable
    *n char(    8 )                   const;                         // Format name
    *n char(   10 )                   const;                         // Call stack entry
    *n int( 10 )                      const;                         // Call stack counter
    *n char(   10 )                   const;                         // Message type
    *n char(    4 )                   const;                         // Message key
    *n int( 10 )                      const;                         // Wait time
    *n char(   10 )                   const;                         // Message action
    *n char( 1024 )                   options( *varsize );           // Error code
  end-pr;

  dcl-ds RCVM0200 qualified len( 4096 );
    Bytes_provided   int( 10 ) inz( %size( RCVM0200 ) );
    Bytes_available  int( 10 );
    Message_ID       char( 7 ) pos( 13 );
    Message_data_len int( 10 ) pos( 153 );
    Message_text_len int( 10 ) pos( 161 );
    Message_data     char( 1 ) pos( 177 );
  end-ds;

  dcl-s  Message_text        char( %size( MsgText ) ) based( ptrMessage_text );
  dcl-s  ptrMessage_text_len pointer;

  // -- API error data structure:
  dcl-ds ERRC0100 qualified;
    BytPrv   int( 10 ) inz( %size( ERRC0100 ));
    BytAvl   int( 10 );
    MsgId    char( 7 );
    *n       char( 1 );
    MsgDta   char( 256 );
  end-ds;

  // Receive last message.

  QMHRCVPM( RCVM0200 : %size( RCVM0200 ) : 'RCVM0200' : '*' : 1 : '*LAST' : '' : 0 : '*SAME' : ERRC0100 );

  if ( ERRC0100.BytAvl = 0 );
    MsgID = RCVM0200.Message_ID;
    ptrMessage_text = %addr( RCVM0200.Message_data ) + RCVM0200.Message_data_len;
    MsgText = %subst( Message_text : 1 : %min( %size( MsgText ) : RCVM0200.Message_text_len ) );
  endif;

  return ( ERRC0100.BytAvl = 0 );

end-proc;
