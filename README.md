# SQL UDTF Tape Label Info #

A SQL user defined table function to get the details for files on a tape.

### What is this repository for? ###

* This SQL function will return details of the files on a tape cartridge.

* The following parameters can be specified:

Parameter                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Tape device                        | varchar(10)                 | Specifies the tape device to read.
Volume ID                          | varchar(8)                 | Specifies the tape in the device. Default value is '*MOUNTED'.
File label                         | varchar(17)                 | Specifies the label of the file. Default value is '*ALL', ie. all files are returned.
Starting file sequence number      | varchar(10)                 | Specifies the first sequence number to return. Default value is '*FIRST'.
Ending file sequence number        | varchar(10)                 | Specifies the last sequence number to return. Default value is '*LAST'.
End option                         | varchar(7)                 | Specifies the operation that is automatically performed on the tape volume after the operation ends.

* The following attributes are returned:

Attribute                           | Data type                     | Description
------------------------------------|:------------------------------|:------------------------------------------
Device name                         | varchar(10)                   | The tape device or tape library device that was used.
Cartridge ID                        | varchar(6)                    | The cartridge identifier for the media. This field will be blanks if the device is not a tape library device.
Label identifier                    | varchar(3)                    | The characters VOL identify this label as a volume label. The system reads this field to verify that it mounts a standard labeled tape. The system also verifies that this label is a volume label. The system writes this field to the tape when you use the Initialize Tape (INZTAP) command and specify the new volume (NEWVOL) parameter.
Label number                        | varchar(1)                    | The relative position of the label within a set of labels of the same type. The label number is always 1 for the IBM standard volume label.
Volume identifier                   | varchar(6)                    | A unique identification code to identify the logical tape volume. The system writes the value that is specified for the new volume (NEWVOL) parameter when you use the Initialize Tape (INZTAP) command. For media library devices, the program recommends that you match the logical volume identifier with the external bar code identifier on the cartridges.
Volume access security              | varchar(3)                    | YES or NO. YES means the volume is secure and the user needs *SECOFR authority to process the volume.
Owner identifier                    | varchar(14)                   | The owner identifier of the tape volume.
Encoding                            | varchar(11)                   | ASCII, EBCDIC or NON-LABELED.
Standard label                      | varchar(3)                    | YES or NO.
Leading tape mark tape              | varchar(3)                    | YES or NO.
Density                             | varchar(10)                   | The density that is used for the data on the tape.
Additional label entries            | varchar(3)                    | YES or NO. Indicates if there were labels on tape that were not returned because the return data would have exceeded 16,000,000 Bytes.
Number of label information entries | integer                       | Number of complete label information entries returned. A value of zero is returned if the tape is not a standard labeled tape, if there are no labels on the tape.
Label 1 Label identifier            | varchar(3)                    | HDR=Header Label (the beginning of a data set), EOF=Trailer Label (the end of a data set), EOV=Trailer Label (the end of a data set that is continued on another volume).
Label 1 Label number                | varchar(1)                    | The relative position of this label, within a set of labels of the same type; it is always 1 for the IBM data set label 1.
Data set identifier                 | varchar(17)                   | A unique identification code to identify the data set (file).
Aggregate volume identifier         | varchar(6)                    | The volume identifier from the volume labels of the first volume in a multivolume data set.
Aggregate volume sequence number    | integer                       | The relative volume number of this volume in a multivolume data set. The field is 0001 for a single volume data set.
Data set sequence number            | integer                       | The relative position of the data set within a multiple data set group.
Creation date                       | date                          | The creation date of the data set in ISO format.
Expiration date                     | date                          | The expiration date of the data set in ISO format.
System code                         | varchar(13)                   | IBMOS400 or 'IBM OS/VS 370'.
Block count                         | integer                       | The number of data blocks in the data set on the current volume.
Label 2 Label identifier            | varchar(3)                    | HDR=Header label (the beginning of a data set), EOF=Trailer label (the end of a data set), EOV=Trailer label (the end of a data set that is continued on another volume).
Label 2 Label number                | varchar(1)                    | The relative positions of this label within the set of labels of the same type. The Label number is always 2 for the IBM data set label 2.
Record format                       | varchar(9)                    | FIXED, VARIABLE or UNDEFINED.
Block length                        | integer                       | The block length (in bytes) of the data blocks on the tape.
Record length                       | integer                       | The record length, in bytes, of the logical records on the tape volume.
Tape density format                 | varchar(8)                    | '1600 bpi', '6250 bpi', '3200 bpi' or OTHER.
Data set position                   | varchar(19)                   | VOLUME_SWITCHED or VOLUME_NOT_SWITCHED.
Compressed                          | varchar(3)                    | YES or NO.
Control character                   | varchar(7)                    | ANSI, MACHINE or NONE.
Block attribute                     | varchar(19)                   | BLOCKED, SPANNED, BLOCKED_AND_SPANNED or NONE.
Checkpoint data set identifier      | varchar(13)                   | CHECKPOINT or NO_CHECKPOINT. Whether the data set is a secure checkpoint data set.

### How do I get set up? ###

* Clone this git repository to a local directory in the IFS, e.g. in your home directory.
* Compile the source using the following CL commands (objects will be placed in the QGPL library):

        CRTSQLRPGI OBJ(QGPL/TAPLABINF) SRCSTMF('taplabinf.sqlrpgle') OBJTYPE(*MODULE)
        CRTSRVPGM  SRVPGM(QGPL/TAPLABINF) EXPORT(*ALL) TEXT('Tape Label Info UDTF')
        RUNSQLSTM  SRCSTMF('udtf_Tape_Label_Info.sql') DFTRDBCOL(QGPL)

* Examples of calling the SQL function:

        select * from table(qgpl.tape_label_info( 'TAP01' )) x                            -- Display all volumes on tape in device TAP01.
        select * from table(qgpl.tape_label_info( 'TAP01', END_OPTION => '*UNLOAD' )) x   -- Display all volumes on tape in device TAP01 and unload tape after operation.
        select * from table(qgpl.tape_label_info( 'TAPMLB01', '123456' )) x               -- Display all volumes on cartridge 123456 in tape library TAPMLB01.

### Documentation ###

[Retrieve Tape Labels (QTARTLBL) API](https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_73/apis/qtartlbl.htm)
